﻿using Chess.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chess.API
{
    public class Helper : ChessContext
    {
        int _widthBoard = 5;
        int _heightBoard = 5;
        string[,] _board;

        public Helper()
        {
            _board = GetBoard();
        }

        /// <summary>
        /// This method for getting ways by man id.
        /// </summary>
        /// <param name="alphabet">The starting alphabet</param>
        /// <param name="manId">Id of man (1=Knight, 2=Bishop)</param>
        /// <returns>List of ways</returns>
        public Response<Way> GetWays(string alphabet, int manId)
        {
            Board input = Board.FirstOrDefault(b => b.Alphabet == alphabet);
            if (input == null)
            {
                throw new ArgumentException($"There is not any {alphabet} in the chess borad.");
            }

            Man man = Man.FirstOrDefault(m => m.Id == manId);
            if (man == null)
            {
                throw new ArgumentException($"There is not any id of man by {manId} in system.");
            }

            List<Way> ways = new List<Way>();
            //Find all of movements that man can go.
            IEnumerable<Movement> movements = Movement.Where(w => w.ManId == manId);
            if (movements.Count() < 0)
            {
                throw new ArgumentException($"There are not any movement of {man.Name}.");
            }

            for (int i = 1; i <= movements.Max(m => m.WayNo); i++)
            {
                bool isValid = true;
                Way way = new Way
                {
                    No = i,
                };
                //Find the sequent way each step
                for (int r = 0; r <= movements.Where(c => c.WayNo == i).Count(); r++)
                {
                    if (isValid)
                    {
                        Step step = new Step();

                        //Step number 0 is itself
                        if (r == 0)
                        {
                            step.Alphabet = _board[input.X, input.Y];
                            step.X = input.X;
                            step.Y = input.Y;
                            way.Steps.Add(step);
                            continue;
                        }

                        Movement movement = movements.FirstOrDefault(f => f.WayNo == i && f.Step == r);
                        
                        if (movement == null ||
                            input.X + movement.X < 0 || //Out of array index
                            input.X + movement.X > _widthBoard || //Out of array index
                            input.Y + movement.Y < 0 || //Out of array index
                            input.Y + movement.Y > _widthBoard || //Out of array index
                            string.IsNullOrWhiteSpace(_board[(input.X + movement.X), input.Y + movement.Y])) //string is not valid
                        {
                            isValid = false;
                            break;
                        }

                        step.X = input.X + movement.X;
                        step.Y = input.Y + movement.Y;
                        step.Alphabet = _board[(input.X + movement.X), input.Y + movement.Y];
                        way.Steps.Add(step);
                    }
                }

                //If invalid in cases out of index array and string is not valid, It will continue.
                if (!isValid) continue;

                //Verify - vowels more than 2
                if (VerifyVowels(way)) ways.Add(way);
            }

            return new Response<Way> { Data = ways }; ;
        }

        private bool VerifyVowels(Way way)
        {
            int countVowel = 0;
            IEnumerable<Vowel> vowels = Vowel.ToList();

            foreach (Step wayStep in way.Steps)
            {
                if (vowels.Any(v => v.Alphabet == wayStep.Alphabet)) countVowel++;
            }

            return countVowel < 2;
        }

        private string[,] GetBoard()
        {
            string[,] board = new string[_widthBoard, _heightBoard];
            for (int x = 0; x < _heightBoard; x++)
            {
                for (int y = 0; y < _widthBoard; y++)
                {
                    board[x, y] = Board.FirstOrDefault(b => b.X == x && b.Y == y).Alphabet;
                }
            }
            return board;
        }
    }
}
