﻿using Chess.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

namespace Chess.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChessesController : ControllerBase
    {
        Helper _helper = new Helper();
        public ChessesController() {}

        /// <summary>
        /// This method for getting ways by man id.
        /// </summary>
        /// <param name="alphabet">The starting alphabet</param>
        /// <param name="manId">Id of man (1=Knight, 2=Bishop)</param>
        /// <returns>List of ways</returns>
        [HttpGet]
        [Route("Alphabets/{alphabet}/Mans/{manId}/Ways")]
        public IActionResult GetWays(string alphabet, int manId)
        {
            //TODO: custom error Handling https://code-maze.com/global-error-handling-aspnetcore/
            try
            {
                var ways = _helper.GetWays(alphabet, manId);
                return Ok(ways);
            }
            catch (System.Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

    }
}