﻿using System;
using System.Collections.Generic;

namespace Chess.API.Models
{
    public partial class Movement
    {
        public int Id { get; set; }
        public int WayNo { get; set; }
        public int Step { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int ManId { get; set; }

        public Man Man { get; set; }
    }
}
