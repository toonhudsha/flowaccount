﻿namespace Chess.API.Models
{
    public class Step
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Alphabet { get; set; }
    }
}
