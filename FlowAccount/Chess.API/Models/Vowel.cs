﻿using System;
using System.Collections.Generic;

namespace Chess.API.Models
{
    public partial class Vowel
    {
        public int Id { get; set; }
        public string Alphabet { get; set; }
    }
}
