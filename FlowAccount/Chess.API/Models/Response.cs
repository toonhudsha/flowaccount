﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chess.API.Models
{
    public class Response<T>
    {
        public List<T> Data { get; set; }
        public int TotalRecords => this.Data.Count;
    }
}
