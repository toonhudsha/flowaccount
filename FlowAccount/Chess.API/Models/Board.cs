﻿using System;
using System.Collections.Generic;

namespace Chess.API.Models
{
    public partial class Board
    {
        public int Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Alphabet { get; set; }
    }
}
