﻿using Microsoft.EntityFrameworkCore;

namespace Chess.API.Models
{
    public partial class ChessContext : DbContext
    {
        public ChessContext()
        {
        }

        public ChessContext(DbContextOptions<ChessContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Board> Board { get; set; }
        public virtual DbSet<Man> Man { get; set; }
        public virtual DbSet<Movement> Movement { get; set; }
        public virtual DbSet<Vowel> Vowel { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.;Database=Chess;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Board>(entity =>
            {
                entity.Property(e => e.Alphabet).HasMaxLength(3);
            });

            modelBuilder.Entity<Man>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Movement>(entity =>
            {
                entity.HasOne(d => d.Man)
                    .WithMany(p => p.Movement)
                    .HasForeignKey(d => d.ManId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ManIdMan");
            });

            modelBuilder.Entity<Vowel>(entity =>
            {
                entity.Property(e => e.Alphabet)
                    .IsRequired()
                    .HasMaxLength(1);
            });
        }
    }
}
