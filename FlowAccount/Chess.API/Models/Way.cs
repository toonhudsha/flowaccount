﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chess.API.Models
{
    public class Way
    {
        public Way()
        {
            Steps = new List<Step>();
        }
        public int No { get; set; }
        public List<Step> Steps { get; set; }
    }
}
