﻿using System.Collections.Generic;

namespace Chess.API.Models
{
    public partial class Man
    {
        public Man()
        {
            Movement = new HashSet<Movement>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Movement> Movement { get; set; }
    }
}
