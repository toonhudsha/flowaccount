﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Program
    {
        private static int[] _result;
        private static int _totalRound = 10;
        static void Main(string[] args)
        {
            _result = new int[_totalRound];

            Fibonacci(1, _totalRound);

            foreach (var item in _result)
            {
                Console.Write(item + ",");
            }

            Console.ReadKey();
        }
        private static void Fibonacci(int round, int _totalRound)
        {
            if (round <= _totalRound)
            {
                if (round >= 3)
                {
                    _result[round - 1] = _result[round - 2] + _result[round - 3];
                    round++;
                    Fibonacci(round, _totalRound);
                    return;
                }
                _result[round - 1] = round - 1;
                round++;
                Fibonacci(round, _totalRound);
            }
        }
    }
}
