﻿CREATE TABLE [dbo].[Movement]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [WayNo] INT NOT NULL, 
    [Step] INT NOT NULL, 
    [X] INT NOT NULL, 
    [Y] INT NOT NULL, 
    [ManId] INT NOT NULL,
	CONSTRAINT FK_ManIdMan FOREIGN KEY (ManId) References [dbo].[Man]
)
