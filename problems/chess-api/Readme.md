Knights on a chessboard
=======================

Given the following miniature chessboard:
<table>
  <tr>
    <td>A</td>
    <td>B</td>
    <td>C</td>
    <td>&nbsp;</td>
    <td>E</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>G</td>
    <td>H</td>
    <td>I</td>
    <td>J</td>
  </tr>
  <tr>
    <td>K</td>
    <td>L</td>
    <td>M</td>
    <td>N</td>
    <td>O</td>
  </tr>
  <tr>
    <td>P</td>
    <td>Q</td>
    <td>R</td>
    <td>S</td>
    <td>T</td>
  </tr>
  <tr>
    <td>U</td>
    <td>V</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Y</td>
  </tr>
</table>

Find the number of unique strings produced by a knight moving from square to square. 
A string is not valid if the knight moves onto a blank square and the string cannot 
contain more than two vowels.

** Write this as a .net-core api with a controller to generate the chess moves and store them onto a DB **
** Your choice of DB can be mongodb,firebase,sql,text-file up to you. **
** Lastly write a postman test to test out if the api endpoint works and that the result is correct :) **